//
// Created by Kisaragi_ on 10/4/21.
//

#ifndef MOODQ_MOODQ_H
#define MOODQ_MOODQ_H

#include "deque"
#include "mutex"

namespace Kisaragi_{
    class Moodq{
    protected:
        std::mutex mtx;
        std::deque<T> dq;
    };
}

#endif //MOODQ_MOODQ_H
