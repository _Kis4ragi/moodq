# μdque
Nothing special, just a simple combination of deque and mutex :D

## Todo:
- [ ] Add access to the deque using a mutex
- [ ] Add unsafe access to the deque without mutex
- [ ] Add overloading for `>>` and `<<` operations
- [ ] Add overloading for others operations